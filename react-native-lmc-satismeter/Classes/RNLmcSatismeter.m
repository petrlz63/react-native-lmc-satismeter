
#import "RNLmcSatismeter.h"
#import <SatisMeter/SatisMeter.h>

@implementation RNLmcSatismeter

- (dispatch_queue_t)methodQueue
{
    return dispatch_get_main_queue();
}
RCT_EXPORT_MODULE()

NSString *_key;


RCT_EXPORT_METHOD(setWriteKey: (NSString *) key)
{
  _key = key;
}

RCT_EXPORT_METHOD(show:(NSString *)name
                  email:(NSString *)email
                  userId:(NSString *)userId
                  createdAt:(NSString *)createdAt
                  language:(NSString *)language
                  ) {



  NSDictionary *traitsDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                    name ,@"name",
                                    email ,@"email",
                                    createdAt, @"createdAt",
                                    language,@"language",
                                    nil];

  [[SatisMeter sharedInstance] identifyUserWithUserId: userId
                                             writeKey: _key
                                  andTraitsDictionary: traitsDictionary];

    
}

@end
