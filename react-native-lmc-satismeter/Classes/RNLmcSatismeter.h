
#if __has_include("RCTBridgeModule.h")
#import "RCTBridgeModule.h"
#else
#import <React/RCTBridgeModule.h>
#endif

@interface RNLmcSatismeter : NSObject <RCTBridgeModule>

@property (nonatomic, strong) NSString *key;

@end
