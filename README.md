# react-native-lmc-satismeter

[![CI Status](https://img.shields.io/travis/Petr Lzicar/react-native-lmc-satismeter.svg?style=flat)](https://travis-ci.org/Petr Lzicar/react-native-lmc-satismeter)
[![Version](https://img.shields.io/cocoapods/v/react-native-lmc-satismeter.svg?style=flat)](https://cocoapods.org/pods/react-native-lmc-satismeter)
[![License](https://img.shields.io/cocoapods/l/react-native-lmc-satismeter.svg?style=flat)](https://cocoapods.org/pods/react-native-lmc-satismeter)
[![Platform](https://img.shields.io/cocoapods/p/react-native-lmc-satismeter.svg?style=flat)](https://cocoapods.org/pods/react-native-lmc-satismeter)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

react-native-lmc-satismeter is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'react-native-lmc-satismeter'
```

## Author

Petr Lzicar, petr.lzicar@lmc.eu

## License

react-native-lmc-satismeter is available under the MIT license. See the LICENSE file for more info.
